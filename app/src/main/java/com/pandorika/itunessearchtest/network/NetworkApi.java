package com.pandorika.itunessearchtest.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandorika.itunessearchtest.models.Tracks;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Oleg Leonov, http://pandorika-it.com on 07.09.15.
 */
public class NetworkApi {
    private static final String SERVER_URL = "https://itunes.apple.com/";

    private Retrofit retrofit;

    public NetworkApi() {
        OkHttpClient client=new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create(new ObjectMapper()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    /*** API для работы с сервером */
    public static ServerApi getServerApi()
    {
        return new NetworkApi().retrofit.create(ServerApi.class);
    }

    public interface ServerApi
    {
        @GET("search")
        Observable<Tracks> searchTracks(@Query("term") String search_string);
    }
}
