package com.pandorika.itunessearchtest.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pandorika.itunessearchtest.R;
import com.pandorika.itunessearchtest.models.Track;

import java.util.List;

/**
 * Created by Leonov Oleg, http://pandorika-it.com on 25.03.16.
 */
public class TrackAdapter extends RecyclerView.Adapter<TrackAdapter.ViewHolder>
{
    private List<Track> tracks;

    public void setTracks(List<Track> tracks)
    {
        this.tracks=tracks;
        notifyDataSetChanged();
    }
    @Override
    public TrackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TrackAdapter.ViewHolder holder, int position) {
        Track track=tracks.get(position);
        holder.artistName.setText(track.artistName);
        holder.collectionName.setText(track.collectionName);
        Glide.with(holder.imageView.getContext())
                .load(track.artworkUrl100)
                .centerCrop().crossFade().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return tracks!=null?tracks.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView imageView;
        public TextView artistName;
        public TextView collectionName;

        public ViewHolder(View v) {
            super(v);
            imageView=(ImageView)v.findViewById(R.id.imageView);
            artistName=(TextView)v.findViewById(R.id.artistName);
            collectionName=(TextView)v.findViewById(R.id.collectionName);
        }
    }
}
