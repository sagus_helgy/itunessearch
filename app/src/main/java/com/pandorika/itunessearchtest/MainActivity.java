package com.pandorika.itunessearchtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;
import com.pandorika.itunessearchtest.adapters.TrackAdapter;
import com.pandorika.itunessearchtest.models.Tracks;
import com.pandorika.itunessearchtest.network.NetworkApi;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final int MIN_SEARCH_STR_LEN=3;//минимальная длина для поиска
    private EditText etSearch;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private TrackAdapter trackAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        etSearch=(EditText)findViewById(R.id.etSearch);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        recyclerView.setAdapter(trackAdapter = new TrackAdapter());

        configureUI();
    }

    /*** Показываем прогресс поиска */
    private void showProgressBar(boolean show)
    {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void configureUI()
    {
        RxTextView.afterTextChangeEvents(etSearch)
                .subscribeOn(AndroidSchedulers.mainThread())
                .debounce(300, TimeUnit.MILLISECONDS)//задержка в 300 миллисекунд, чтобы не замучать сервер запросами
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Func1<TextViewAfterTextChangeEvent, Boolean>() {
                    @Override
                    public Boolean call(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                        //Меньше MIN_SEARCH_STR_LEN не отправляем запрос
                        boolean needSendRequest = textViewAfterTextChangeEvent.editable().toString().length() >= MIN_SEARCH_STR_LEN;
                        if (!needSendRequest) trackAdapter.setTracks(null);
                        return needSendRequest;
                    }
                })
                .flatMap(new Func1<TextViewAfterTextChangeEvent, Observable<Tracks>>() {
                    @Override
                    public Observable<Tracks> call(TextViewAfterTextChangeEvent textView) {
                        showProgressBar(true);
                        return NetworkApi.getServerApi().searchTracks(textView.editable().toString())
                                .observeOn(AndroidSchedulers.mainThread())
                                .onErrorResumeNext(new Func1<Throwable, Observable<? extends Tracks>>() {
                                    @Override
                                    public Observable<? extends Tracks> call(Throwable e) {
                                        //Обработка ошибки, чтобы поток не разорвался
                                        e.printStackTrace();
                                        if (e instanceof UnknownHostException)
                                            Toast.makeText(MainActivity.this, R.string.error_no_internet, Toast.LENGTH_SHORT).show();
                                        else
                                            Toast.makeText(MainActivity.this, R.string.error_unknown, Toast.LENGTH_SHORT).show();
                                        showProgressBar(false);
                                        trackAdapter.setTracks(null);
                                        return Observable.<Tracks>empty();
                                    }
                                });
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Tracks>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        //Обработка ошибок идёт в onErrorResumeNext
                    }

                    @Override
                    public void onNext(Tracks trackList) {
                        trackAdapter.setTracks(trackList.results);
                        showProgressBar(false);
                    }
                });
    }
}
