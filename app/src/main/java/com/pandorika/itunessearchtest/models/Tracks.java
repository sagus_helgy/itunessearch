package com.pandorika.itunessearchtest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Leonov Oleg, http://pandorika-it.com on 25.03.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tracks {
    public int resultCount;
    public List<Track> results;
}
