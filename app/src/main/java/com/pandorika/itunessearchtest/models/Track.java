package com.pandorika.itunessearchtest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Leonov Oleg, http://pandorika-it.com on 25.03.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Track {
    public String artistName;
    public String collectionName;
    public String artworkUrl100;
}
